<?php
/**
 * Front page template.
 *
 * @package alexlavigin/pr-theme
 */

get_header();

$ads_top    = carbon_get_theme_option( 'pr_ads_header' );
$execute_id = [];
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599532071712792',
							'class_name' => 'desktop',
							'position'   => 'top',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599529984725254',
							'class_name' => 'mobile',
							'position'   => 'top',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="dfr">
						<h2><?php esc_html_e( 'Новости', 'pr' ); ?></h2>
					</div>
					<?php get_template_part( 'template-parts/news', 'block' ); ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'trends' ); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_166262865202243177',
							'class_name' => 'desktop',
							'position'   => 'middle',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_166262907544629035',
							'class_name' => 'mobile',
							'position'   => 'middle',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'interview' ); ?>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'slider' ); ?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_166262910742713771',
							'class_name' => 'desktop',
							'position'   => 'bottom',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_166262909139686647',
							'class_name' => 'mobile',
							'position'   => 'bottom',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'production' ); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/main', 'vacancies' ); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'case' ); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php get_template_part( 'template-parts/news', 'tenders' ); ?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
