<?php
/**
 * Footer template.
 *
 * @package PR
 */

$socials    = carbon_get_theme_option( 'pr_socials' );
$ads_footer = carbon_get_theme_option( 'pr_ads_footer' );

if ( ! empty( $ads_footer ) ) :
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="advertising">
					<?php echo $ads_footer; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
				<div class="telegram-link">
					<i class="icon-arrow"></i>
					<p><?php echo esc_html( carbon_get_theme_option( 'pr_footer_text_subscribe' ) ); ?></p>
					<a
							class="button"
							href="<?php echo esc_html( carbon_get_theme_option( 'pr__footer_button_link' ) ); ?>">
						<?php echo esc_html( carbon_get_theme_option( 'pr_footer_button_text' ) ); ?>
					</a>
				</div>
				<p class="copyright">
					<?php echo esc_html( carbon_get_theme_option( 'pr_copyright' ) ); ?>
				</p>
				<i class="icon-plus-18"></i>
				<?php if ( ! empty( $socials ) ) : ?>
					<ul class="soc">
						<?php foreach ( $socials as $social ) : ?>
							<li class="<?php echo esc_attr( $social['pr_soc_icon_name'] ); ?>">
								<a href="<?php echo esc_attr( $social['pr_soc_link'] ); ?>"></a>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
				<?php if ( has_nav_menu( 'header_menu' ) ) : ?>
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'header_menu',
							'menu_class'     => 'menu',
							'container'      => '',
							'echo'           => true,
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
					?>
				<?php endif; ?>
				<?php if ( has_nav_menu( 'footer_menu_two_column' ) ) : ?>
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'footer_menu_two_column',
							'menu_class'     => 'menu',
							'container'      => '',
							'echo'           => true,
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
					?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<?php echo carbon_get_theme_option( 'pr_footer_script' ); ?>
</body>
</html>