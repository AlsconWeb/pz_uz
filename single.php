<?php
/**
 * Single Post template.
 *
 * @package PR.
 */

get_header();
$socials    = carbon_get_theme_option( 'pr_socials' );
$execute_id = [];
?>
	<section>
		<div class="container">
			<?php
			if ( ! empty( $ads_top ) ) :
				get_template_part(
					'template-parts/ads',
					'header',
					[
						'ads_top' => $ads_top,
					]
				);
			endif;
			?>
			<div class="row dfr">
				<?php if ( have_posts() ) : ?>
					<?php
					while ( have_posts() ) :
						the_post();
						$execute_id[] = get_the_ID();
						?>
						<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
							<div class="blog-inner">
								<p class="date"><?php do_action( 'pr_the_time', $post->ID ); ?></p>
								<h1 class="title"><?php the_title(); ?></h1>
								<?php the_excerpt(); ?>
								<?php if ( has_post_thumbnail( $execute_id[0] ) ) : ?>
									<?php the_post_thumbnail( 'pr_big_banner', [ 'class' => 'thumbnails' ] ); ?>
									<?php if ( get_the_post_thumbnail_caption( $post->ID ) ) : ?>
										<p class="caption">
											<?php esc_html_e( 'Фото:', 'pr' ); ?>
											<?php the_post_thumbnail_caption(); ?>
										</p>
									<?php endif; ?>
								<?php else : ?>
									<img
											src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
											alt="No Image">
								<?php endif; ?>
								<?php the_content(); ?>
								<?php if ( ! empty( $socials ) ) : ?>
									<ul class="soc">
										<?php foreach ( $socials as $social ) : ?>
											<li class="<?php echo esc_attr( $social['pr_soc_icon_name'] ); ?>">
												<a href="<?php echo esc_attr( $social['pr_soc_link'] ); ?>"></a>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h3 class="title"><?php esc_html_e( 'Читайте также:', 'pr' ); ?></h3>
								</div>
								<?php
								$term_post = wp_get_post_terms( $execute_id[0], 'category' )[0];

								$arg = [
									'post_type'      => 'post',
									'posts_per_page' => 3,
									'cat'            => $term_post->term_id,
									'post_status'    => 'publish',
									'post__not_in'   => $execute_id,
								];

								$read_more_post = new WP_Query( $arg );

								if ( $read_more_post->have_posts() ) :
									while ( $read_more_post->have_posts() ) :
										$read_more_post->the_post();
										?>
										<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
											<div class="blog-item">
												<?php if ( has_post_thumbnail( get_the_ID() ) ) : ?>
													<?php the_post_thumbnail( 'pr_small_thumb' ); ?>
												<?php else : ?>
													<img
															src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
															alt="No Image">
												<?php endif; ?>
												<div class="description">
													<h2 class="title"><?php the_title(); ?></h2>
													<p class="date"><?php do_action( 'pr_the_time', get_the_ID() ); ?></p>
												</div>
												<a class="link" href="<?php the_permalink(); ?>"></a>
											</div>
										</div>
									<?php
									endwhile;
									wp_reset_postdata();
									?>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php
					get_template_part(
						'template-parts/popular',
						'posts',
						[
							'execute'        => $execute_id,
							'read_more'      => true,
							'posts_per_page' => 9,
						]
					);
					?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
