jQuery( document ).ready( function( $ ) {
	$( '#order_by' ).change( function( e ) {
		$( '#pr_city_select_h' ).val( $( '#pr_city_select' ).val() );

		let work_diary = [];
		work_diary.push( $( '#work_diary' ).val() );
		work_diary.push( $( '#remotely:checked' ).val() );
		$( '#work_diary_h' ).val( work_diary )

		$( '#pr_min_salary_h' ).val( $( '#pr_min_salary' ).val() )

		$( '#pr_only_salary_h' ).val( $( '#pr_only_salary:checked' ).val() );

		let specializationEl = $( '.specialization:checked' );
		let specializationData = [];

		$.each( specializationEl, function( i, el ) {
			specializationData.push( $( el ).val() )
		} );

		$( '#pr_specialization_h' ).val( specializationData );

		$( '#search-from' ).submit();
	} );

	$( '.link-to-vacansia input' ).val( $( '.button.green.back-to-job' ).attr( 'href' ) );

	let el = $( '.modern-title' );
	
	if ( el.length ) {
		el.text( 'Нажмите или перетащите файл в эту область, чтобы загрузить его.' )
	}
} );