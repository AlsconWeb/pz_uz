<?php
/**
 * Single job's template.
 *
 * @package alexlavigin/pr-theme
 */

use PR\JobFilter;
use PR\ThemeInit;

get_header( 'jobs' );

$socials      = carbon_get_theme_option( 'pr_socials' );
$company_tem  = wp_get_post_terms( get_the_ID(), 'company', [ 'fields' => 'all' ] )[0];
$company_logo = carbon_get_term_meta( $company_tem->term_id, 'pr_company_logo' );
$company_url  = carbon_get_term_meta( $company_tem->term_id, 'pr_company_website' );

$filter = new JobFilter();
// phpcs:disable
$from_output = isset( $_GET['form-application'] ) ? filter_var( wp_unslash( $_GET['form-application'] ), FILTER_SANITIZE_NUMBER_INT ) : false;
// phpcs:enable
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599532071712792',
							'class_name' => 'desktop',
							'position'   => 'top',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599529984725254',
							'class_name' => 'mobile',
							'position'   => 'top',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<?php
					if ( ! $from_output && have_posts() ) {
						while ( have_posts() ) {
							the_post();
							$job_id        = get_the_ID();
							$job_salary    = ThemeInit::get_salary( $job_id );
							$condition     = carbon_get_post_meta( $job_id, 'pr_condition' );
							$city          = wp_get_post_terms( $job_id, 'location', [ 'fields' => 'all' ] )[0];
							$job_condition = ThemeInit::get_condition( $job_id );
							$work_day      = carbon_get_post_meta( $job_id, 'pr_working' );
							$phone         = carbon_get_post_meta( $job_id, 'pr_phone' );
							$phone_url     = filter_var( $phone, FILTER_SANITIZE_NUMBER_INT );
							$email         = carbon_get_post_meta( $job_id, 'pr_email' );
							$url_form      = get_the_permalink() . '?form-application=' . $job_id;
							?>
							<div class="description">
								<h1><?php the_title(); ?></h1>
								<p class="date"><?php the_time( 'd F' ); ?></p>
								<span class="sline"></span>
								<div class="dfr">
									<p class="price"><?php esc_attr_e( 'Зарплата', 'pr' ); ?>
										<span><?php echo esc_html( $job_salary ); ?></span>
									</p>
									<div class="send-application">
										<a href="<?php echo esc_url( $url_form ); ?>" class="button green send-app">
											<?php esc_attr_e( 'Откликнуться на вакансию', 'pr' ); ?>
										</a>
									</div>
								</div>
								<div class="requirements">
									<p><?php esc_attr_e( 'Требования', 'pr' ); ?></p>
									<?php
									if ( ! empty( $condition ) ) {
										?>
										<ul class="dfr">
											<?php foreach ( $condition as $item ) { ?>
												<li><?php echo esc_html( $item['title'] ); ?></li>
											<?php } ?>
										</ul>
										<?php
									}
									?>
								</div>
								<div class="location">
									<p><?php esc_attr_e( 'Местоположение и занятость', 'rp' ); ?></p>
									<ul class="dfr">
										<li><?php echo esc_html( $city->name ); ?></li>
										<li><?php echo esc_html( ThemeInit::get_work_day( $work_day ) ); ?></li>
									</ul>
								</div>
								<span class="sline"></span>
								<div class="content">
									<?php the_content(); ?>
								</div>
								<span class="sline"> </span>
								<div class="soc dfr">
									<p><?php esc_attr_e( 'Поделиться вакансией', 'pr' ); ?></p>
									<?php if ( ! empty( $socials ) ) { ?>
										<ul class="soc dfr">
											<?php foreach ( $socials as $social ) { ?>
												<li class="<?php echo esc_attr( $social['pr_soc_icon_name'] ); ?>">
													<a href="<?php echo esc_attr( $social['pr_soc_link'] ); ?>"></a>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>
							</div>

							<div class="contacts">
								<h3>Контакты </h3>
								<div class="tel">
									<?php if ( ! empty( $phone ) ) { ?>
										<p><?php echo wp_kses_post( __( 'Если вы заинтересовались данной <br>вакансией, свяжитесь с нами по номеру:', 'pr' ) ); ?></p>
										<a href="tel:+<?php echo esc_attr( $phone_url ); ?>"><?php echo esc_html( $phone ); ?></a>
									<?php } ?>
								</div>
								<div class="mail">
									<p><?php echo wp_kses_post( __( 'или пришлите ваше резюме на нашу<br>электронную почту:', 'pr' ) ); ?></p>
									<a href="mailto:<?php echo esc_html( $email ); ?>"><?php echo esc_html( $email ); ?></a>
								</div>
							</div>
							<?php
						}
					} else {
						$job_id     = get_the_ID();
						$phone      = carbon_get_post_meta( $job_id, 'pr_phone' );
						$phone_url  = filter_var( $phone, FILTER_SANITIZE_NUMBER_INT );
						$email      = carbon_get_post_meta( $job_id, 'pr_email' );
						$job_salary = ThemeInit::get_salary( $job_id );
						?>
						<div class="description form-summary">
							<div class="dfr">
								<h1><?php esc_attr_e( 'Ваше резюме', 'pr' ); ?></h1>
								<a href="<?php the_permalink(); ?>" class="button green back-to-job">
									<?php esc_attr_e( 'Вернутся к объявлению', 'pr' ); ?>
								</a>
							</div>
							<p class="title-job"><?php echo wp_kses_post( __( 'Для отклика на вакансию', 'pr' ) . ' <span>' . get_the_title() . ' ' . $job_salary ) . '</span>'; ?> </p>
							<hr class="short-line">
							<h2><?php esc_attr_e( 'Контактные данные', 'pr' ); ?></h2>
							<?php echo do_shortcode( '[wpforms id="5211"]' ); ?>
						</div>
						<div class="contacts">
							<h3>Контакты </h3>
							<div class="tel">
								<?php if ( ! empty( $phone ) ) { ?>
									<p><?php echo wp_kses_post( __( 'Если вы заинтересовались данной <br>вакансией, свяжитесь с нами по номеру:', 'pr' ) ); ?></p>
									<a href="tel:+<?php echo esc_attr( $phone_url ); ?>"><?php echo esc_html( $phone ); ?></a>
								<?php } ?>
							</div>
							<div class="mail">
								<p><?php echo wp_kses_post( __( 'или пришлите ваше резюме на нашу<br>электронную почту:', 'pr' ) ); ?></p>
								<a href="mailto:<?php echo esc_html( $email ); ?>"><?php echo esc_html( $email ); ?></a>
							</div>
						</div>
						<?php
					}
					?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="company">
						<a href="<?php echo esc_url( get_term_link( $company_tem->term_id ) ); ?>">
							<?php if ( ! empty( $company_logo ) ) { ?>
								<img
										src="<?php echo esc_url( $company_logo ); ?>"
										alt="<?php echo esc_html( 'logo ' . $company_tem->name ); ?>">
							<?php } else { ?>
								<img src="https://via.placeholder.com/70x50" alt="No company logo">
							<?php } ?>
						</a>
						<h3 class="title"><?php echo esc_html( $company_tem->name ); ?></h3>
						<p><?php echo wp_kses_post( $company_tem->description ); ?></p>
						<a href="<?php echo esc_url( $company_url ); ?>"><?php echo esc_html( $company_url ); ?></a>
					</div>
					<?php
					$filter->show_filter();
					get_template_part( 'template-parts/company', 'vacancies' );
					?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
