<?php
/**
 * Template Name: Company's.
 *
 * @package alexlavigin/pr-theme
 */

use PR\JobFilter;

get_header( 'jobs' );

$filter = new JobFilter();
?>
	<div class="ads">
		<!--AdFox START-->
		<!--WebSail-Advertisement-->
		<!--Площадка: PR.UZ / * / *-->
		<!--Тип баннера: 640x300-->
		<!--Расположение: верх страницы-->
		<div id="adfox_16599529984725254"></div>
		<script>
			window.yaContextCb.push( () => {
				Ya.adfoxCode.create( {
					ownerId: 277709,
					containerId: 'adfox_16599529984725254',
					params: {
						pp: 'g',
						ps: 'fyae',
						p2: 'geyc',
						puid1: ''
					}
				} )
			} )
		</script>
		<!--AdFox START-->
		<!--WebSail-Advertisement-->
		<!--Площадка: PR.UZ / * / *-->
		<!--Тип баннера: 1170x250-->
		<!--Расположение: верх страницы-->
		<div id="adfox_16599532071712792"></div>
		<script>
			window.yaContextCb.push( () => {
				Ya.adfoxCode.create( {
					ownerId: 277709,
					containerId: 'adfox_16599532071712792',
					params: {
						pp: 'g',
						ps: 'fyae',
						p2: 'hpfv',
						puid1: ''
					}
				} )
			} )
		</script>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="add-jobs dfr">
						<div class="desc">
							<h1 class="title"><?php esc_html_e( 'Компании', 'pr' ); ?></h1>
						</div>
						<a
								class="button"
								href="<?php echo esc_url( carbon_get_theme_option( 'pr_jobs_button_link' ) ) ?? '#'; ?>">
							<?php esc_html_e( 'Добавить компанию', 'pr' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<div class="company-vacancies">
						<?php
						$companys = get_terms(
							[
								'taxonomy'   => 'company',
								'hide_empty' => true,
								'fields'     => 'all',
							]
						);

						if ( ! empty( $companys ) ) {
							foreach ( $companys as $key => $company ) {
								$company_logo    = carbon_get_term_meta( $company->term_id, 'pr_company_logo' );
								$premium         = carbon_get_post_meta( $company->term_id, 'pr_premium' );
								$company_site    = carbon_get_term_meta( $company->term_id, 'pr_company_website' );
								$premium_class   = '';
								$company_premium = '';
								if ( carbon_get_term_meta( $company->term_id, 'pr_premium' ) ) {
									$company_premium = 'data-title="' . __( 'Premium', 'pr' ) . '"';
									$premium_class   = 'premium';
								}

								?>
								<div class="company-item <?php echo esc_html( $premium_class ); ?>">
									<?php if ( ! empty( $company_logo ) ) { ?>
										<img
												src="<?php echo esc_url( $company_logo ); ?>"
												alt="<?php echo esc_html( 'logo ' . $company->name ); ?>">
									<?php } else { ?>
										<img src="https://via.placeholder.com/70x50" alt="No company logo">
									<?php } ?>
									<div class="desc">
										<h5 class="title" <?php echo esc_html( $company_premium ); ?>>
											<?php echo esc_html( $company->name ); ?>
										</h5>
										<p class="description">
											<?php esc_html( term_description( $company->term_id ) ); ?>
										</p>
										<p class="number-vacancies">
											<?php
											echo esc_html(
												sprintf(
												/* translators: %s Count jobs */
													_n( '%s вакансия', '%s вакансий', $company->count ),
													$company->count
												)
											);
											?>
										</p>
										<a
												class="link"
												href="<?php echo esc_url( get_term_link( $company->term_id ) ); ?>"></a>
									</div>
									<a class="link-page" href="<?php echo esc_url( $company_site ); ?>">
										<?php echo esc_url( $company_site ); ?>
									</a>
								</div>
							<?php if ( 4 === $key ) { ?>
								<!--AdFox START-->
								<!--WebSail-Advertisement-->
								<!--Площадка: PR.UZ / * / *-->
								<!--Тип баннера: 1170x250-->
								<!--Расположение: середина страницы-->
								<div id="adfox_166150583794359128"></div>
								<script>
									window.yaContextCb.push( () => {
										Ya.adfoxCode.create( {
											ownerId: 277709,
											containerId: 'adfox_166150583794359128',
											params: {
												pp: 'h',
												ps: 'fyae',
												p2: 'hpfv',
												puid1: ''
											}
										} )
									} )
								</script>
							<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php
					$filter->show_filter();
					get_template_part( 'template-parts/company', 'vacancies' );
					?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
