<?php
/**
 * Taxonomy Company template.
 *
 * @package alexlavigin/pr-theme
 */

use PR\ThemeInit;

get_header( 'jobs' );

$company_term                = get_queried_object();
$company_id                  = $company_term->term_id;
$company_name                = $company_term->name;
$company_premium             = '';
$company_logo_url            = carbon_get_term_meta( $company_id, 'pr_company_logo' );
$company_site                = carbon_get_term_meta( $company_id, 'pr_company_website' );
$company_specialization      = carbon_get_term_meta( $company_id, 'pr_specialization' );
$company_phone               = carbon_get_term_meta( $company_id, 'pr_phone' );
$company_email               = carbon_get_term_meta( $company_id, 'pr_email' );
$company_address             = carbon_get_term_meta( $company_id, 'pr_address' );
$company_address_link_google = carbon_get_term_meta( $company_id, 'pr_address_link_google' );
$company_description         = term_description( $company_id );
$vacancies_title             = __( 'Открытые вакансии в ', 'pr' ) . $company_name;

$premium_class   = '';
$company_premium = '';
$iteration       = 0;

if ( carbon_get_term_meta( $company_id, 'pr_premium' ) ) {
	$company_premium = 'data-title="' . __( 'Premium', 'pr' ) . '"';
	$premium_class   = 'premium';
}

$args = [
	'tax_query'      => [
		[
			'taxonomy' => 'company',
			'field'    => 'id',
			'terms'    => $company_id,
		],
	],
	'posts_per_page' => 4,
];

$company_query = new WP_Query( $args );

?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599532071712792',
							'class_name' => 'desktop',
							'position'   => 'top',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599529984725254',
							'class_name' => 'mobile',
							'position'   => 'top',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<div class="dfr">
						<img
								src="<?php echo esc_url( $company_logo_url ); ?>"
								alt="<?php echo esc_attr( $company_premium ); ?>">
						<h2 class="title" <?php echo esc_attr( $company_premium ); ?>>
							<?php echo esc_html( $company_name ); ?>
						</h2>
					</div>
					<hr>
					<ul>
						<li>
							<p><?php esc_html_e( 'Cпециализация:', 'pr' ); ?></p>
							<?php echo wp_kses_post( wpautop( $company_specialization ) ); ?>
						</li>
						<li>
							<p><?php esc_html_e( 'Сайт:', 'pr' ); ?></p>
							<a href="<?php echo esc_url( $company_site ); ?>"><?php echo esc_url( $company_site ); ?></a>
						</li>
						<li>
							<p><?php esc_html_e( 'Телефон:', 'pr' ); ?></p>
							<a href="tel:<?php echo esc_html( $company_phone ); ?>">
								<?php echo esc_html( $company_phone ); ?>
							</a>
						</li>
						<li>
							<p><?php esc_html_e( 'Email:', 'pr' ); ?></p>
							<a href="mailto:<?php echo esc_html( $company_email ); ?>">
								<?php echo esc_html( $company_email ); ?>
							</a>
						</li>
						<li>
							<p><?php esc_html_e( 'Адрес:', 'pr' ); ?></p>
							<a href="<?php echo esc_url( $company_address_link_google ); ?>">
								<?php echo esc_html( $company_address ); ?>
							</a>
						</li>
					</ul>
					<h2><?php esc_html_e( 'Описание:', 'pr' ); ?></h2>
					<?php echo wp_kses_post( wpautop( $company_description ) ); ?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="vacancies">
						<h3><?php echo esc_html( $vacancies_title ); ?></h3>
						<?php
						if ( $company_query->have_posts() ) {

						while ( $company_query->have_posts() ) {
							$company_query->the_post();
							$job_id     = get_the_ID();
							$city       = wp_get_post_terms( $job_id, 'location', [ 'fields' => 'all' ] )[0];
							$job_salary = ThemeInit::get_salary( $job_id );
							?>
							<div class="vacancie-item">
								<h3><?php the_title(); ?></h3>
								<p class="price"><?php echo esc_html( $job_salary ); ?></p>
								<p class="city"><?php echo esc_html( $city->name ?? '' ); ?></p>
								<a class="link" href="<?php the_permalink(); ?>"></a>
							</div>
						<?php
						$iteration ++;
						if ( 4 === $iteration ){
						?>
							<!--AdFox START-->
							<!--WebSail-Advertisement-->
							<!--Площадка: PR.UZ / * / *-->
							<!--Тип баннера: 1170x250-->
							<!--Расположение: середина страницы-->
							<div id="adfox_166150583794359128"></div>
							<script>
								window.yaContextCb.push( () => {
									Ya.adfoxCode.create( {
										ownerId: 277709,
										containerId: 'adfox_166150583794359128',
										params: {
											pp: 'h',
											ps: 'fyae',
											p2: 'hpfv',
											puid1: ''
										}
									} )
								} )
							</script>
						<?php
						}
						}
						wp_reset_postdata();
						?>

							<a
									class="button"
									href="<?php echo esc_url( get_bloginfo( 'url' ) . '/jobs/filter/company-in-' . $company_term->slug ); ?>">
								<?php esc_html_e( 'Все вакансии', 'pr' ); ?>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();

