<?php
/**
 * Post Views counter.
 *
 * @package PR
 */

namespace PR;

/**
 * PostViews class file.
 */
class PostViews {

	public const META_KEY_NAME = 'pr_views';

	public const EXCLUDE_BOTS = true;

	/**
	 * PostViews construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init Hooks.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'wp_head', [ $this, 'rp_add_post_views' ] );
		add_action( 'admin_head', [ $this, 'add_cron_delete_counter' ] );
		add_filter( 'cron_schedules', [ $this, 'cron_add_one_week' ] );
		add_action( 'pr_counter_cron', [ $this, 'delete_all_counter' ] );
	}

	/**
	 * Add count views to post.
	 *
	 * @param array $args Arguments.
	 *
	 * @return void
	 */
	public function rp_add_post_views( $args = [] ): void {
		global $user_ID, $post, $wpdb;

		if ( ! $post || ! is_singular() ) {
			return;
		}

		$rg = (object) wp_parse_args(
			$args,
			[
				'meta_key'     => self::META_KEY_NAME,
				'who_count'    => 1,
				'exclude_bots' => self::EXCLUDE_BOTS,
			]
		);

		$do_count = false;
		switch ( $rg->who_count ) {

			case 0:
				$do_count = true;
				break;
			case 1:
				if ( ! $user_ID ) {
					$do_count = true;
				}
				break;
			case 2:
				if ( $user_ID ) {
					$do_count = true;
				}
				break;
		}

		if ( $do_count && $rg->exclude_bots ) {

			$not_bot    = 'Mozilla|Opera';
			$bot        = 'Bot/|robot|Slurp/|yahoo';
			$user_agent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_STRING ) : '';

			if ( ! preg_match( "/$not_bot/i", $user_agent ) || preg_match( "~$bot~i", $user_agent ) ) {
				$do_count = false;
			}
		}

		if ( $do_count ) {

			$up = $wpdb->query(
				$wpdb->prepare(
					"UPDATE $wpdb->postmeta SET meta_value = (meta_value+1) WHERE post_id = %d AND meta_key = %s",
					$post->ID,
					$rg->meta_key
				)
			);

			if ( ! $up ) {
				add_post_meta( $post->ID, $rg->meta_key, 1, true );
			}

			wp_cache_delete( $post->ID, 'post_meta' );
		}
	}

	/**
	 * Get Post View count.
	 *
	 * @param int $post_id Post ID.
	 *
	 * @return mixed|string
	 */
	public static function get_pr_post_views( $post_id ) {
		return get_post_meta( $post_id, self::META_KEY_NAME, true );
	}

	/**
	 * Delete all view counter in post.
	 *
	 * @return void
	 */
	public function delete_all_counter(): void {
		global $wpdb;

		$meta_key = self::META_KEY_NAME;

		//phpcs:disable
		$wpdb->query(
			$wpdb->prepare(
				"DELETE FROM `{$wpdb->prefix}postmeta` WHERE `meta_key` = %s;",
				$meta_key
			)
		);
		//phpcs:enable
	}

	/**
	 * Add cron job.
	 *
	 * @return void
	 */
	public function add_cron_delete_counter(): void {
		if ( ! wp_next_scheduled( 'pr_counter_cron' ) ) {
			wp_schedule_event( time(), 'one_week', 'pr_counter_cron' );
		}
	}

	/**
	 * Add cron time.
	 *
	 * @param array $schedules schedules.
	 *
	 * @return mixed
	 */
	public function cron_add_one_week( array $schedules ): array {
		$schedules['one_week'] = [
			'interval' => 7 * DAY_IN_SECONDS,
			'display'  => 'Раз в неделю',
		];

		return $schedules;
	}
}
