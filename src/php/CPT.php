<?php
/**
 * Register CPT
 *
 * @package alexlavigin/pr-theme
 */

namespace PR;

/**
 * CPT class file.
 */
class CPT {
	/**
	 * CPT construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init function.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'init', [ $this, 'register_cpt' ] );
		add_action( 'init', [ $this, 'create_taxonomy' ] );
	}

	/**
	 * Register CPT.
	 *
	 * @return void
	 */
	public function register_cpt(): void {
		register_post_type(
			'jobs',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Вакансии', 'pr' ),
					'singular_name'      => __( 'Вакансия', 'pr' ),
					'add_new'            => __( 'Добавить вакансию', 'pr' ),
					'add_new_item'       => __( 'Добавление вакансии', 'pr' ),
					'edit_item'          => __( 'Редактирование вакансию', 'pr' ),
					'new_item'           => __( 'Новая вакансия', 'pr' ),
					'view_item'          => __( 'Смотреть вакансию', 'pr' ),
					'search_items'       => __( 'Искать вакнсию', 'pr' ),
					'not_found'          => __( 'Не найдено', 'pr' ),
					'not_found_in_trash' => __( 'Не найдено в корзине', 'pr' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Вакансии', 'pr' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 10,
				'menu_icon'     => 'dashicons-welcome-write-blog',
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'author',
					'thumbnail',
				],
				'taxonomies'    => [ 'location', 'specialization', 'company' ],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Create taxonomy.
	 *
	 * @return void
	 */
	public function create_taxonomy(): void {
		register_taxonomy(
			'location',
			[ 'jobs' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Город', 'pr' ),
					'singular_name'     => __( 'Город', 'pr' ),
					'search_items'      => __( 'Найти город', 'pr' ),
					'all_items'         => __( 'Все Города', 'pr' ),
					'view_item '        => __( 'Посмотреть Город', 'pr' ),
					'parent_item'       => __( 'Город', 'pr' ),
					'parent_item_colon' => __( 'Город', 'pr' ),
					'edit_item'         => __( 'Редактировать Город', 'pr' ),
					'update_item'       => __( 'Обновить Город', 'pr' ),
					'add_new_item'      => __( 'Добавить Город', 'pr' ),
					'new_item_name'     => __( 'Новый Город', 'pr' ),
					'menu_name'         => __( 'Города', 'pr' ),
					'back_to_items'     => __( '← Вернутся в Города', 'pr' ),
				],
				'description'       => __( 'Место расположение вакансии', 'pr' ),
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'query_var'         => 'location',
				'show_admin_column' => true,
				'capabilities'      => [],
			]
		);

		register_taxonomy(
			'specialization',
			[ 'jobs' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Специализация', 'pr' ),
					'singular_name'     => __( 'Специализация', 'pr' ),
					'search_items'      => __( 'Найти Специализацию', 'pr' ),
					'all_items'         => __( 'Все Специализации', 'pr' ),
					'view_item '        => __( 'Посмотреть Специализации', 'pr' ),
					'parent_item'       => __( 'Специализация', 'pr' ),
					'parent_item_colon' => __( 'Специализация', 'pr' ),
					'edit_item'         => __( 'Редактировать Город', 'pr' ),
					'update_item'       => __( 'Обновить Специализацию', 'pr' ),
					'add_new_item'      => __( 'Добавить Специализацию', 'pr' ),
					'new_item_name'     => __( 'Новый Специализация', 'pr' ),
					'menu_name'         => __( 'Специализацию', 'pr' ),
					'back_to_items'     => __( '← Вернутся в Специализация', 'pr' ),
				],
				'description'       => __( 'Специализацию которая требуется', 'pr' ),
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'query_var'         => 'specialization',
				'show_admin_column' => true,
				'capabilities'      => [],
			]
		);

		register_taxonomy(
			'company',
			[ 'jobs' ],
			[
				'label'        => '',
				'labels'       => [
					'name'              => __( 'Компании', 'pr' ),
					'singular_name'     => __( 'Компания', 'pr' ),
					'search_items'      => __( 'Найти Компанию', 'pr' ),
					'all_items'         => __( 'Все Компании', 'pr' ),
					'view_item '        => __( 'Посмотреть Компанию', 'pr' ),
					'parent_item'       => __( 'Компания', 'pr' ),
					'parent_item_colon' => __( 'Компания', 'pr' ),
					'edit_item'         => __( 'Редактировать Компанию', 'pr' ),
					'update_item'       => __( 'Обновить Компанию', 'pr' ),
					'add_new_item'      => __( 'Добавить Компанию', 'pr' ),
					'new_item_name'     => __( 'Новый Компания', 'pr' ),
					'menu_name'         => __( 'Компании', 'pr' ),
					'back_to_items'     => __( '← Вернутся в Компаниям', 'pr' ),
				],
				'description'  => __( 'Компании которые размещают вакансию', 'pr' ),
				'public'       => true,
				'hierarchical' => true,
				'rewrite'      => true,
				'query_var'    => 'company',
				'capabilities' => [],
			]
		);
	}
}
