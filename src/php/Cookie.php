<?php
/**
 * Cookie helpers.
 *
 * @package alexlavigin/pr-theme
 */

namespace PR;

/**
 * Cookie class file.
 */
class Cookie {

	/**
	 * Filter cookie name.
	 */
	const FILTER_COOKIE_NAME = 'pr-jobs-filter';


	/**
	 * Get filter cookie.
	 *
	 * @return object
	 */
	public static function get_filter_cookie() {
		$value = self::get( self::FILTER_COOKIE_NAME );

		if ( null !== $value ) {
			return $value;
		}

		return (object) [];
	}

	/**
	 * Get cookie.
	 *
	 * @param string $name Cookie name.
	 *
	 * @return object|null
	 */
	private static function get( $name ) {
		$cookie = isset( $_COOKIE[ $name ] ) ?
			sanitize_text_field( wp_unslash( $_COOKIE[ $name ] ) ) :
			'';

		return json_decode( $cookie );
	}

	/**
	 * Set cookie.
	 *
	 * @param string $name  Cookie name.
	 * @param mixed  $value Cookie value.
	 *
	 * @return void
	 */
	public static function set( $name, $value ): void {

		if ( is_array( $value ) ) {
			$value = wp_json_encode( $value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE );
		}

		setcookie( $name, $value, strtotime( '+30 days' ), COOKIEPATH, COOKIE_DOMAIN );

		$_COOKIE[ $name ] = $value;
	}
}
