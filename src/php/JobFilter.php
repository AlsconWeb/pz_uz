<?php
/**
 * Job filter.
 *
 * @package alexlavigin/pr-theme
 */

namespace PR;

use WP_Query;

/**
 * JobFilter class file.
 */
class JobFilter {

	/**
	 * Select work diary array.
	 *
	 * @var array
	 */
	public array $work_diary;

	/**
	 * Nonce cod name.
	 */
	public const PR_FILTER_NONCE_NAME = 'pr_filter_submit';

	/**
	 * Serach filter options.
	 *
	 * @var array
	 */
	private $serach_form_options;

	/**
	 * Code Match.
	 *
	 * @var string[] Invalid characters.
	 */
	private array $code_match = [
		'"',
		'!',
		'@',
		'#',
		'$',
		'%',
		'^',
		'&',
		'*',
		'(',
		')',
		'_',
		'+',
		'{',
		'}',
		'|',
		':',
		'"',
		'<',
		'>',
		'?',
		'[',
		']',
		';',
		"'",
		',',
		'.',
		'/',
		'',
		'~',
		'`',
		'=',
	];


	/**
	 * JobFilter construct.
	 */
	public function __construct() {
		$this->work_diary = [
			'full'     => __( 'Полный рабочий день', 'pr' ),
			'shift'    => __( 'Cменный график', 'pr' ),
			'flexible' => __( 'Гибкий график', 'pr' ),
			'remote'   => __( 'Удаленная работа', 'pr' ),
		];

		$this->serach_form_options = [
			'date'       => __( 'По дате размещения', 'pr' ),
			'asc-salary' => __( 'По возрастанию зарплаты', 'pr' ),
			'dsc-salary' => __( 'По убыванию зарплаты', 'pr' ),
		];

		$this->url_rewrite_rules();

		add_action( 'pre_get_posts', [ $this, 'add_query_filter' ] );
		add_filter( 'query_vars', [ $this, 'add_filter_vars' ] );

		add_action( 'admin_post_nopriv_pr_jobs_filter', [ $this, 'prod_filter_handler' ] );
		add_action( 'admin_post_pr_jobs_filter', [ $this, 'prod_filter_handler' ] );
	}


	/**
	 * Update Main query product from filter.
	 *
	 * @param WP_Query $query Main Query.
	 *
	 * @return WP_Query
	 */
	public function add_query_filter( WP_Query $query ): WP_Query {

		if (
			! is_admin() && $query->is_main_query() &&
			is_post_type_archive( 'jobs' ) &&
			! empty( $query->query_vars['filter'] )
		) {
			$filter_params  = $this->parse_url_query( $query->query_vars['filter'] );
			$specialization = [];
			$city           = '';
			$only_salary    = 0;
			$work_diary     = '';
			$min_salary     = '';
			$order_by       = '';


			foreach ( $filter_params as $key => $param ) {
				switch ( $key ) {
					case 'specialization':
						$specialization[] = $param;
						break;
					case 'city':
						$city = $param;
						break;
					case 'only_salary':
						$only_salary = 1;
						break;
					case 'work_diary':
						$work_diary = $param;
						break;
					case 'min_salary':
						$min_salary = $param;
						break;
					case 'order_by':
						$order_by = $param[1];
						break;
				}
			}


			if ( ! empty( $specialization ) ) {
				$query_tax = [
					'relation' => 'AND',
					[
						'taxonomy' => 'specialization',
						'field'    => 'slug',
						'terms'    => $specialization[0],
					],
				];
			}

			if ( ! empty( $city ) ) {
				$query_tax[] = [
					'taxonomy' => 'location',
					'field'    => 'slug',
					'terms'    => $city,
				];
			}

			$meta_query = [];

			if ( ! empty( $only_salary ) && empty( $min_salary ) ) {
				$meta_query[] = [
					'key'   => 'salary',
					'value' => 1,
				];
			}

			if ( ! empty( $min_salary ) ) {
				$meta_query['relation'] = 'AND';
				$meta_query[]           = [
					'compare' => '>=',
					'type'    => 'NUMERIC',
					'key'     => '_pr_salary_min',
					'value'   => (int) $min_salary[1],
				];
			}

			if ( ! empty( $work_diary ) ) {
				$meta_query[] = [
					'meta_compare_key' => 'LIKE',
					'key'              => '_pr_working',
					'value'            => $work_diary,
				];
			}

			if ( ! empty( $order_by ) && 'asc-salary' === $order_by ) {
				$query->set( 'order', 'ASC' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'meta_key', '_pr_salary_min' );
			}

			if ( ! empty( $order_by ) && 'dsc_salary' === $order_by ) {
				$query->set( 'order', 'DESC' );
				$query->set( 'orderby', 'meta_value_num' );
				$query->set( 'meta_key', '_pr_salary_min' );
			}

			if ( ! empty( $order_by ) && 'date' === $order_by ) {
				$query->set( 'order', 'DESC' );
				$query->set( 'orderby', 'date' );
			}

			if ( ! empty( $query_tax ) ) {
				$query->set( 'tax_query', $query_tax );
			}

			if ( ! empty( $meta_query ) ) {
				$query->set( 'meta_query', $meta_query );
			}

			return $query;
		}

		return $query;
	}


	/**
	 * Show filter.
	 *
	 * @return void
	 */
	public function show_filter(): void {
		$filter    = $this->parse_url_query( get_query_var( 'filter' ) ) ?? [];
		$check_box = '';
		if ( ! empty( $filter['work_diary'] ) && 1 < count( $filter['work_diary'] ) ) {
			$work_selected = $filter['work_diary'][0];
			$check_box     = $filter['work_diary'][1];
		} elseif ( ! empty( $filter['work_diary'] ) ) {
			$work_selected = $filter['work_diary'][1];
		}
		?>
		<form
				class="form-jobs"
				method="post"
				id="pr_filter_jobs"
				action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
			<p class="close"><?php esc_html_e( 'Фильтры', 'pr' ); ?> <i class="icon-close"></i></p>
			<div class="select">
				<label for="pr_city_select"><?php esc_html_e( 'Город', 'pr' ); ?></label>
				<select name="pr_jobs_filter[city][]" id="pr_city_select">
					<?php foreach ( $this->get_city_option() as $city ) { ?>
						<option value="<?php echo esc_attr( $city['value'] ); ?>" <?php isset( $filter['city'] ) ? selected( $filter['city'][1], $city['value'] ) : ''; ?>>
							<?php echo esc_html( $city['name'] ); ?>
						</option>
					<?php } ?>
				</select>
			</div>
			<div class="select">
				<label for="work_diary"><?php esc_html_e( 'Тип занятости', 'pr' ); ?></label>
				<select name="pr_jobs_filter[work_diary][]" id="work_diary">
					<option value="0"><?php esc_html_e( 'Любой', 'pr' ); ?></option>
					<?php foreach ( $this->work_diary as $key => $value ) { ?>
						<option value="<?php echo esc_attr( $key ); ?>" <?php echo isset( $filter['work_diary'] ) && $work_selected === $key ? 'selected' : ''; ?>>
							<?php echo esc_html( $value ); ?>
						</option>
					<?php } ?>
				</select>
			</div>
			<div class="checkbox">
				<input
						id="remotely"
						type="checkbox"
						name="pr_jobs_filter[work_diary][]"
						value="remote"
					<?php echo ! empty( $check_box ) ? 'checked' : ''; ?>
				>
				<label class="icon-checked" for="remotely"><?php esc_html_e( 'Можно удалённо', 'pr' ); ?></label>
			</div>
			<div class="input">
				<label for="min_salary"><?php esc_html_e( 'Зарплата', 'pr' ); ?></label>
				<input
						type="text"
						name="pr_jobs_filter[min_salary]"
						id="pr_min_salary"
						value="<?php echo ! empty( $filter['min_salary'][1] ) ? esc_attr( $filter['min_salary'][1] ) : ''; ?>"
						placeholder="<?php esc_html_e( 'От', 'pr' ); ?>">
			</div>
			<div class="checkbox">
				<input
						id="pr_only_salary"
						name="pr_jobs_filter[only_salary]" <?php echo ! empty( $filter['only_salary'] ) ? 'checked' : ''; ?>
						type="checkbox" value="true">
				<label
						class="icon-checked"
						for="pr_only_salary"><?php esc_html_e( 'Только с указанием зарплаты', 'pr' ); ?></label>
			</div>
			<div class="checkboxs">
				<p><?php esc_html_e( 'Специализация', 'pr' ); ?></p>
				<?php foreach ( $this->get_specialization() as $value ) { ?>
					<div class="checkbox">
						<input
								id="marketing-<?php echo esc_attr( $value['value'] ); ?>"
								class="specialization"
								name="pr_jobs_filter[specialization][]"
								value="<?php echo esc_attr( $value['value'] ); ?>"
							<?php echo isset( $filter['specialization'] ) && in_array( $value['value'], $filter['specialization'], true ) ? 'checked' : ''; ?>
								type="checkbox">
						<label class="icon-checked" for="marketing-<?php echo esc_attr( $value['value'] ); ?>">
							<?php echo esc_html( $value['name'] ); ?>
						</label>
					</div>
				<?php } ?>
			</div>
			<input
					class="button" type="submit" name="pr_submit"
					value="<?php esc_html_e( 'Применить фильтр', 'pr' ); ?>">
			<input type="hidden" name="action" value="pr_jobs_filter">
			<input
					type="hidden" name="company"
					value="<?php echo ! empty( $filter['company'][1] ) ? esc_attr( $filter['company'][1] ) : ''; ?>">
			<?php wp_nonce_field( self::PR_FILTER_NONCE_NAME, self::PR_FILTER_NONCE_NAME ); ?>
			<a class="reset" href="<?php echo esc_url( get_post_type_archive_link( 'jobs' ) ); ?>">
				<?php esc_html_e( 'Сбросить', 'pr' ); ?>
			</a>
		</form>
		<?php
	}

	/**
	 * Show search filter.
	 *
	 * @return void
	 */
	public function show_search_form(): void {
		global $wp_the_query;

		$count = $wp_the_query->found_posts;

		$filter = $this->parse_url_query( get_query_var( 'filter' ) ) ?? [];
		?>
		<div class="search">
			<form class="dfr" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" id="search-from">
				<a class="filter" href="#"><?php esc_html_e( 'Фильтры', 'pr' ); ?></a>
				<div class="select">
					<select name="pr_jobs_filter[order_by][]" id="order_by">
						<?php
						foreach ( $this->serach_form_options as $key => $option ) {
							$value = esc_attr( $key );
							?>
							<option value="<?php echo esc_attr( $key ); ?>" <?php ! empty( $filter['order_by'] ) ? selected( $value, $filter['order_by'][1] ) : ''; ?>>
								<?php echo esc_html( $option ); ?>
							</option>
						<?php } ?>
					</select>
				</div>
				<input type="hidden" name="action" value="pr_jobs_filter">
				<input
						type="hidden" name="pr_jobs_filter[city][]" id="pr_city_select_h"
						value="<?php echo ! empty( $filter['city'][1] ) ? esc_attr( $filter['city'][1] ) : ''; ?>">
				<input
						type="hidden" name="pr_jobs_filter[work_diary][]" id="work_diary_h"
						value="<?php echo ! empty( $filter['work_diary'] ) ? esc_attr( implode( ',', $filter['work_diary'] ) ) : ''; ?>">
				<input
						type="hidden" name="pr_jobs_filter[min_salary][]"
						id="pr_min_salary_h"
						value="<?php echo ! empty( $filter['min_salary'] ) ? esc_attr( (int) $filter['min_salary'] ) : ''; ?>">
				<input
						type="hidden" name="pr_jobs_filter[specialization][]" id="pr_specialization_h"
						value="<?php echo ! empty( $filter['specialization'] ) ? esc_attr( implode( ',', $filter['specialization'] ) ) : ''; ?>">
				<input
						type="hidden" name="pr_jobs_filter[only_salary]" id="pr_only_salary_h"
						value="<?php echo ! empty( $filter['only_salary'] ) ? 'true' : ''; ?>">
				<input type="hidden" name="pr_jobs_filter[search_from]" id="pr_search_from_h" value="true">
				<?php wp_nonce_field( self::PR_FILTER_NONCE_NAME, self::PR_FILTER_NONCE_NAME ); ?>
			</form>
			<?php echo sprintf( '<p>%s %s %s</p>', esc_html( __( 'Найдено', 'pr' ) ), esc_attr( $count ), esc_html( __( 'вакансий', 'pr' ) ) ); ?>
		</div>
		<?php

	}

	/**
	 * Get city array from select.
	 *
	 * @return array
	 */
	public function get_city_option(): array {
		$city_terms = get_terms(
			[
				'taxonomy'   => 'location',
				'hide_empty' => true,
			]
		);

		if ( empty( $city_terms ) ) {
			return [
				'value' => '0',
				'name'  => __( 'Выберите город', 'pr' ),
			];
		}

		$city_options   = [];
		$city_options[] = [
			'value' => 0,
			'name'  => __( 'Выберите город', 'pr' ),
		];

		foreach ( $city_terms as $city_term ) {
			$city_options[] = [
				'value' => $city_term->slug,
				'name'  => $city_term->name,
			];
		}

		return $city_options;
	}

	/**
	 * Get specialization.
	 *
	 * @return array
	 */
	public function get_specialization(): array {
		$specialization_terms = get_terms(
			[
				'taxonomy'   => 'specialization',
				'hide_empty' => true,
			]
		);

		if ( empty( $specialization_terms ) ) {
			return [];
		}

		$specialization_options = [];
		foreach ( $specialization_terms as $specialization ) {
			$specialization_options[] = [
				'value' => $specialization->slug,
				'name'  => $specialization->name,
			];
		}

		return $specialization_options;
	}

	/**
	 * Parse string URL
	 *
	 * @param string $url_query URL Query.
	 *
	 * @return array|false
	 */
	public function parse_url_query( string $url_query ) {
		$param_string = $url_query;
		$query_arg    = [];
		if ( ! empty( $param_string ) ) {
			$params = explode( '-and-', $param_string );
			foreach ( $params as $param ) {
				$items = explode( '-in-', urldecode( $param ) );
				if ( empty( $items[1] ) ) {
					continue;
				}
				$param_name = $items[0];
				unset( $items[0] );
				if ( preg_match( '-or-', $items[1] ) ) {
					$query_arg[ $param_name ] = explode( '-or-', $items[1] );
				} else {
					$query_arg[ $param_name ] = $items;
				}
			}

			return array_filter( $query_arg );
		}

		return false;
	}

	/**
	 * Generate Url Redirect
	 */
	public function prod_filter_handler(): void {
		if ( ! isset( $_POST[ self::PR_FILTER_NONCE_NAME ] ) && ! wp_verify_nonce( self::PR_FILTER_NONCE_NAME, self::PR_FILTER_NONCE_NAME ) ) {
			wp_safe_redirect( filter_input( INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING ) );
		}

		$request = ! empty( $_REQUEST['pr_jobs_filter'] ) ? wp_unslash( $_REQUEST['pr_jobs_filter'] ) : [];
		$request = array_filter( $request );

		$params = [];
		foreach ( $request as $key => $item ) {

			if ( ( $request['search_from'] && 'work_diary' === $key ) || ( $request['search_from'] && 'specialization' === $key ) ) {
				$item = explode( ',', $item[0] );
			}

			$item = array_filter( (array) $item );
			if ( ! empty( $item ) ) {
				$params[ $key ] = [];
				foreach ( $item as $value ) {
					$params[ $key ][] = rawurlencode( str_replace( $this->code_match, '-', $value ) );
				}
			}
		}

		if ( empty( $params ) ) {
			$redirect = get_bloginfo( 'url' ) . '/jobs/';
			wp_safe_redirect( $redirect . '/' );
			die();
		}

		$redirect = get_bloginfo( 'url' ) . '/jobs/filter/';
		$i        = 0;
		foreach ( $params as $key => $param ) {
			if ( 0 === $i ) {
				$redirect .= $key . '-in-' . implode( '-or-', (array) $param );
				$i ++;
				continue;
			}

			$param = array_filter( $param );

			// Why it is different to the above?
			foreach ( $param as $index => $item ) {
				$addon = ( 0 === $index ) ? '-and-' . $key . '-in-' : '-or-';

				$redirect .= $addon . $item;
			}
		}
		wp_safe_redirect( $redirect . '/' );
		die();
	}

	/**
	 * Add Rewrite rules url
	 */
	public function url_rewrite_rules(): void {

		add_rewrite_rule(
			'jobs/filter/([-_a-zA-Z0-9+%.:]+)/page/(\d+)/?$',
			'index.php?post_type=jobs&filter=$matches[1]&paged=$matches[2]',
			'top'
		);

		add_rewrite_rule(
			'jobs/filter/([-_a-zA-Z0-9+%.:]+)/?$',
			'index.php?post_type=jobs&filter=$matches[1]',
			'top'
		);
	}

	/**
	 * Add Query vars filter
	 *
	 * @param array $vars Vars Query.
	 *
	 * @return array
	 */
	public function add_filter_vars( array $vars ): array {
		$vars[] = 'filter';

		return $vars;
	}
}
