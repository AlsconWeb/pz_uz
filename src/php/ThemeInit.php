<?php
/**
 * Theme Init.
 *
 * @package alexlavigin/pr-theme
 */

namespace PR;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * ThemeInit class file.
 */
class ThemeInit {

	/**
	 * Theme version.
	 */
	public const PR_VERSION = '1.3.5';

	/**
	 * Select work diary array.
	 *
	 * @var array
	 */
	public array $work_diary;

	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		$this->init_hook();
		new PostViews();
		new CPT();
		new JobFilter();

		$this->work_diary = [
			'full'     => __( 'Полный рабочий день', 'pr' ),
			'shift'    => __( 'Cменный график', 'pr' ),
			'flexible' => __( 'Гибкий график', 'pr' ),
			'remote'   => __( 'Удаленная работа', 'pr' ),
		];
	}

	/**
	 * Init Themes hooks
	 *
	 * @return void
	 */
	public function init_hook(): void {
		add_action( 'after_setup_theme', [ $this, 'crb_load' ] );
		add_action( 'after_setup_theme', [ $this, 'theme_supports' ] );
		add_filter( 'upload_mimes', [ $this, 'add_support_svg_uploads' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options_page' ] );
		add_action( 'pr_the_time', [ $this, 'get_pr_the_time' ] );
		add_action( 'pre_get_posts', [ $this, 'change_template_blog' ] );
		add_action( 'save_post_jobs', [ $this, 'save_jobs_post' ] );
	}

	/**
	 * Boot Carbon fields.
	 *
	 * @return void
	 */
	public function crb_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * After theme setup.
	 *
	 * @return void
	 */
	public function theme_supports(): void {

		// add support custom logo.
		add_theme_support(
			'custom-logo',
			[
				'height'               => 27,
				'width'                => 185,
				'flex-width'           => true,
				'flex-height'          => true,
				'header-text'          => '',
				'unlink-homepage-logo' => false,
			]
		);
		add_theme_support( 'align-wide' );

		// add menu support.
		register_nav_menus(
			[
				'header_menu'            => __( 'Меню в шапке', 'pr' ),
				'header_menu_jobs'       => __( 'Меню в шапке Jobs', 'pr' ),
				'footer_menu'            => __( 'Меню в подвале первая колонка', 'pr' ),
				'footer_menu_two_column' => __( 'Меню в подвале вторая колонка', 'pr' ),
			]
		);

		// add image size.
		add_image_size( 'pr_big_banner', 700, 460, [ 'left', 'center' ] );
		add_image_size( 'pr_thumb', 380, 252, [ 'left', 'center' ] );
		add_image_size( 'pr_small_thumb', 180, 120, [ 'left', 'center' ] );
		add_image_size( 'pr_asd_thumb', 120, 80, [ 'left', 'center' ] );
		add_image_size( 'pr_prod_thumb', 480, 316, [ 'left', 'center' ] );
		add_image_size( 'pr_news_small_thumb', 135, 90, [ 'left', 'center' ] );
		add_image_size( 'pr_medium_thumb', 200, 123, [ 'left', 'center' ] );
		add_image_size( 'pr_tenders_thumb', 280, 185, [ 'left', 'center' ] );

		// add post format.
		add_theme_support(
			'post-formats',
			[
				'gallery',
				'image',
				'quote',
				'video',
				'audio',
			]
		);
		add_post_type_support( 'post', 'post-formats' );
		add_theme_support( 'post-thumbnails', [ 'post' ] );
	}

	/**
	 * Add support upload svg.
	 *
	 * @param array $file_types file types.
	 *
	 * @return array
	 */
	public function add_support_svg_uploads( array $file_types ): array {

		$file_types['svg'] = 'image/svg+xml';

		return $file_types;
	}

	/**
	 * Add Style and Scripts.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		wp_enqueue_style( 'pr-main-style', get_stylesheet_directory_uri() . '/assets/css/main.css', [], self::PR_VERSION );
		wp_enqueue_style( 'pr-style', get_stylesheet_directory_uri() . '/style.css', [], self::PR_VERSION );
		wp_enqueue_style( 'pr-google-font', '//fonts.googleapis.com/css2?family=Oswald:wght@400;500;600;700&amp;Montserrat:wght@600&amp;family=Roboto:wght@400;500;700&amp;display=swap', [], self::PR_VERSION );
		wp_enqueue_style( 'ostd-html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0' );
		wp_enqueue_style( 'ostd-respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2' );

		wp_style_add_data( 'ostd-html5shiv', 'conditional', 'IE 9' );
		wp_style_add_data( 'ostd-respond', 'conditional', 'IE 9' );

		wp_enqueue_script( 'pr-build', get_stylesheet_directory_uri() . '/assets/js/build.js', [ 'jquery' ], self::PR_VERSION, true );
		wp_enqueue_script( 'pr-main', get_stylesheet_directory_uri() . '/assets/js/main.js', [ 'jquery' ], self::PR_VERSION, true );
	}

	/**
	 * Add Theme Options page.
	 *
	 * @return void
	 */
	public function add_theme_options_page(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Настройки темы', 'pr' ) )
			->add_fields(
				[
					Field::make( 'header_scripts', 'pr_header_script', __( 'Скрипт в шапке', 'pr' ) ),
					Field::make( 'footer_scripts', 'pr_footer_script', __( 'Скрипт в подвале', 'pr' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Шапка', 'pr' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'pr_button_text', __( 'Текст кнопки', 'pr' ) )->set_width( 50 ),
					Field::make( 'text', 'pr_button_link', __( 'Ссылка кнопки', 'pr' ) )->set_width( 50 ),
					Field::make( 'text', 'pr_jobs_button_link', __( 'Ссылка кнопки подать новую вакансию', 'pr' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Подвал', 'pr' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'pr_footer_text_subscribe', __( 'Текст подписки', 'pr' ) ),
					Field::make( 'text', 'pr_footer_button_text', __( 'Текст кнопки', 'pr' ) )->set_width( 50 ),
					Field::make( 'text', 'pr__footer_button_link', __( 'Ссылка кнопки', 'pr' ) )->set_width( 50 ),
					Field::make( 'textarea', 'pr_copyright', __( 'Копирайт', 'pr' ) )->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', __( 'Социальные сети', 'pr' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'complex', 'pr_socials', __( 'Социальная сеть', 'pr' ) )
						->add_fields(
							[
								Field::make( 'text', 'pr_soc_icon_name', __( 'Название иконки', 'pr' ) )
									->set_width( 50 ),
								Field::make( 'text', 'pr_soc_link', __( 'Ссылка', 'pr' ) )->set_width( 50 ),
							]
						),
				]
			);

		Container::make( 'theme_options', __( 'Реклама', 'pr' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'pr_ads_header', __( 'Код вставки в шапке', 'pr' ) )->set_rows( 4 ),
					Field::make( 'textarea', 'pr_ads_footer', __( 'Код вставки в подвале', 'pr' ) )->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', __( 'Страница 404', 'pr' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'textarea', 'pr_404_block_first', __( 'Первый блок текста', 'pr' ) )->set_rows( 4 ),
					Field::make( 'textarea', 'pr_404_block_second', __( 'Второй блок текста', 'pr' ) )->set_rows( 4 ),
				]
			);

		Container::make( 'post_meta', __( 'Видео баннер', 'pr' ) )
			->where( 'post_type', '=', 'post' )
			->add_fields(
				[
					Field::make( 'textarea', 'pr_embed_video', __( 'Видео iframe', 'pr' ) ),
				]
			);

		Container::make( 'post_meta', __( 'Вакансия', 'pr' ) )
			->where( 'post_type', '=', 'jobs' )
			->add_fields(
				[
					Field::make( 'checkbox', 'pr_premium', __( 'Premium', 'pr' ) ),
					Field::make( 'separator', 'pr_style_salary', __( 'Зарплата и требование', 'pr' ) ),
					Field::make( 'text', 'pr_salary_min', __( 'От', 'pr' ) )
						->set_attribute( 'placeholder', __( 'От', 'pr' ) )
						->set_width( 50 ),
					Field::make( 'text', 'pr_salary_max', __( 'До', 'pr' ) )
						->set_attribute( 'placeholder', __( 'До', 'pr' ) )
						->set_width( 50 ),
					Field::make( 'complex', 'pr_condition', __( 'Требования', 'pr' ) )
						->add_fields(
							[
								Field::make( 'text', 'title', __( 'Текс', 'pr' ) ),
							]
						),

					Field::make( 'multiselect', 'pr_working', __( 'Вид занятости', '' ) )
						->add_options( $this->work_diary ),

					Field::make( 'separator', 'pr_style_contact', __( 'Контактная информация', 'pr' ) ),
					Field::make( 'text', 'pr_phone', __( 'Телефон', 'pr' ) )
						->set_attribute( 'placeholder', '+*** (**) ***-**-**' )->set_width( 50 ),
					Field::make( 'text', 'pr_email', __( 'Email', 'pr' ) )->set_width( 50 ),
				]
			);

		Container::make( 'term_meta', __( 'Айдентика компании', 'pr' ) )
			->where( 'term_taxonomy', '=', 'company' )
			->add_fields(
				[
					Field::make( 'checkbox', 'pr_premium', __( 'Premium', 'pr' ) ),
					Field::make( 'image', 'pr_company_logo', __( 'Логотип', 'pr' ) )->set_width( 50 )
						->set_value_type( 'url' ),
					Field::make( 'text', 'pr_company_website', __( 'Ссылка на сайт', 'pr' ) ),
					Field::make( 'textarea', 'pr_specialization', __( 'Cпециализация', 'pr' ) )
						->set_attribute( 'placeholder', 'Банк, финансовые операции' ),
					Field::make( 'text', 'pr_phone', __( 'Телефон', 'pr' ) ),
					Field::make( 'text', 'pr_email', __( 'Email', 'pr' ) ),
					Field::make( 'text', 'pr_address', __( 'Адрес', 'pr' ) ),
					Field::make( 'text', 'pr_address_link_google', __( 'Ссылка на гугл карту', 'pr' ) ),

				]
			);
	}

	/**
	 * Output date in format (14:22 to day )
	 *
	 * @param int $post_id post id.
	 */
	public function get_pr_the_time( int $post_id ) {
		$time_stamp = get_the_time( 'Y-m-d', $post_id );

		if ( gmdate( 'Y-m-d' ) === $time_stamp ) {
			echo esc_html( get_the_time( 'H:i' ) . ', ' . __( 'сегодня', 'pr' ) );

			return true;
		}

		echo esc_html( get_the_time( 'd F', $post_id ) );

	}


	/**
	 * Add filter change template patch.
	 *
	 * @return void
	 */
	public function change_template_blog(): void {
		// phpcs:disable
		// Processing form data without nonce verification.
		if ( isset( $_GET['popular'] ) ) {
			add_filter( 'template_include', [ $this, 'get_template_popular_post' ] );
		}
		//phpcs:enable
	}

	/**
	 * Get popular post template.
	 *
	 * @param string $template Template.
	 *
	 * @return string
	 */
	public function get_template_popular_post( string $template ): string {
		return locate_template( [ 'popular-posts.php' ] );
	}

	/**
	 * Get work day.
	 *
	 * @param array $values from select.
	 *
	 * @return string
	 */
	public static function get_work_day( array $values ): string {
		$work_day = [];
		foreach ( $values as $value ) {
			switch ( $value ) {
				case 'full':
					$work_day[] = __( 'Полный рабочий день', 'pr' );
					break;
				case 'shift':
					$work_day[] = __( 'Cменный график', 'pr' );
					break;
				case 'flexible':
					$work_day[] = __( 'Гибкий график', 'pr' );
					break;
				case 'remote':
					$work_day[] = __( 'Удаленная работа', 'pr' );
					break;
				default:
					$work_day[] = __( 'Полный рабочий день', 'pr' );
			}
		}

		return implode( ' • ', $work_day );
	}

	/**
	 * Get condition string.
	 *
	 * @param int $job_id Job id.
	 *
	 * @return string
	 */
	public static function get_condition( int $job_id ): string {
		$condition       = carbon_get_post_meta( $job_id, 'pr_condition' );
		$condition_array = [];

		if ( empty( $condition ) ) {
			return '';
		}

		foreach ( $condition as $value ) {
			$condition_array[] = $value['title'];
		}

		return implode( ' • ', $condition_array );
	}

	/**
	 * Get job salary
	 *
	 * @param int $job_id Job id.
	 *
	 * @return string
	 */
	public static function get_salary( int $job_id ): string {
		$min_salary = filter_var( carbon_get_post_meta( $job_id, 'pr_salary_min' ), FILTER_SANITIZE_NUMBER_FLOAT );
		$max_salary = filter_var( carbon_get_post_meta( $job_id, 'pr_salary_max' ), FILTER_SANITIZE_NUMBER_FLOAT );

		if ( empty( $min_salary ) ) {
			return __( 'Зарплата не указана', 'pr' );
		}

		if ( empty( $max_salary ) ) {
			return __( 'от', 'pr' ) . ' ' . number_format( (float) $min_salary, 0, ' ', ' ' );
		}

		return __( 'от', 'pr' ) . ' ' . number_format( (float) $min_salary, 0, ' ', ' ' ) . ' ' . __( 'до', 'pr' ) . ' ' . number_format( (float) $max_salary, 0, ' ', ' ' ) . ' ' . __( 'сум', 'pr' );
	}

	/**
	 * Save post job.
	 *
	 * @param int|string $post_id Post id.
	 *
	 * @return void
	 */
	public function save_jobs_post( $post_id ) {

		if ( ! empty( $_POST['carbon_fields_compact_input']['_pr_salary_min'] ) ) {
			update_post_meta( $post_id, 'salary', true );
		}
	}

}
