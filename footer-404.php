<?php
/**
 * Footer 404 Template.
 *
 * @package PR
 */

?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
				<div class="dfr">
					<div class="telegram-link">
						<p><?php echo wp_kses_post( carbon_get_theme_option( 'pr_footer_text_subscribe' ) ); ?></p>
						<a
								class="button"
								href="<?php echo esc_html( carbon_get_theme_option( 'pr__footer_button_link' ) ); ?>">
							<?php echo esc_html( carbon_get_theme_option( 'pr_footer_button_text' ) ); ?>
						</a>
					</div>
					<p class="copyright"><?php echo esc_html( carbon_get_theme_option( 'pr_copyright' ) ); ?></p>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<?php echo carbon_get_theme_option( 'pr_footer_script' ); ?>
</body>
</html>
