<?php
/**
 * Header 404 Template
 *
 * @package PR
 */

$socials = carbon_get_theme_option( 'pr_socials' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
	<?php echo carbon_get_theme_option( 'pr_header_script' ); ?>
</head>
<body <?php body_class( 'error404' ); ?> >
<header>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="dfr">
					<a class="logo" href="<?php bloginfo( 'url' ); ?>">
						<?php
						$logo = get_theme_mod( 'custom_logo' );
						if ( $logo ) :
							?>
							<img
									src="<?php echo esc_url( wp_get_attachment_image_url( $logo, 'full' ) ); ?>"
									alt="Logo"
									width="195px">
						<?php endif; ?>
					</a>
					<i class="icon-plus-18"></i>
					<?php if ( ! empty( $socials ) ) : ?>
						<ul class="soc">
							<?php foreach ( $socials as $social ) : ?>
								<li class="<?php echo esc_attr( $social['pr_soc_icon_name'] ); ?>">
									<a href="<?php echo esc_attr( $social['pr_soc_link'] ); ?>"></a>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>
