<?php
/**
 * Archive Jobs Template.
 *
 * @package alexlavigin/pr-theme
 */

use PR\JobFilter;
use PR\ThemeInit;

get_header( 'jobs' );

$filter    = new JobFilter();
$iteration = 0;
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599532071712792',
							'class_name' => 'desktop',
							'position'   => 'top',
						]
					);

					get_template_part(
						'template-parts/ads',
						'block',
						[
							'block_id'   => 'adfox_16599529984725254',
							'class_name' => 'mobile',
							'position'   => 'top',
						]
					);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="add-jobs dfr">
						<div class="desc">
							<h1 class="title"><?php esc_html_e( 'Вакансии', 'pr' ); ?></h1>
						</div>
						<a
								class="button"
								href="<?php echo esc_url( carbon_get_theme_option( 'pr_jobs_button_link' ) ) ?? '#'; ?>">
							<?php esc_html_e( 'Добавить вакансию', 'pr' ); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<?php $filter->show_search_form(); ?>
					<div class="company-vacancies">
						<?php
						if ( have_posts() ) {
							while ( have_posts() ) {
								the_post();
								$job_id        = get_the_ID();
								$company_tem   = wp_get_post_terms( $job_id, 'company', [ 'fields' => 'all' ] )[0];
								$company_logo  = carbon_get_term_meta( $company_tem->term_id, 'pr_company_logo' );
								$city          = wp_get_post_terms( $job_id, 'location', [ 'fields' => 'all' ] )[0];
								$work_day      = carbon_get_post_meta( $job_id, 'pr_working' );
								$job_condition = ThemeInit::get_condition( $job_id );
								$job_salary    = ThemeInit::get_salary( $job_id );
								$premium       = carbon_get_post_meta( $job_id, 'pr_premium' );
								?>
								<div class="vacancy <?php echo $premium ? 'premium' : ''; ?>" <?php echo $premium ? 'data-title="Premium"' : ''; ?>>
									<div class="dfr">
										<?php if ( ! empty( $company_logo ) ) { ?>
											<img
													src="<?php echo esc_url( $company_logo ); ?>"
													alt="<?php echo esc_html( 'logo ' . $company_tem->name ); ?>">
										<?php } else { ?>
											<img src="https://via.placeholder.com/70x50" alt="No company logo">
										<?php } ?>
										<h5 class="title"><?php echo esc_html( $company_tem->name ?? '' ); ?></h5>
										<p class="date"><?php the_time( 'd F' ); ?></p>
									</div>
									<h3 class="title"><?php the_title(); ?></h3>
									<p class="city">
										<?php echo esc_html( $city->name ?? '' ); ?>
										• <?php echo esc_html( ThemeInit::get_work_day( $work_day ) ); ?>
									</p>
									<p class="price"><?php echo esc_html( $job_salary ); ?></p>
									<p class="description"><?php echo esc_html( $job_condition ); ?></p>
									<a class="link" href="<?php the_permalink(); ?>"></a>
								</div>
								<?php

								$iteration ++;

								if ( 4 === $iteration ) {
									get_template_part(
										'template-parts/ads',
										'block',
										[
											'block_id'   => 'adfox_166150583794359128',
											'class_name' => 'desktop',
											'position'   => 'middle',
										]
									);
								}
							}
						}
						?>
					</div>
					<div class="page_navigation_wrapper">
						<?php
						if ( function_exists( 'wp_pagenavi' ) ) {
							wp_pagenavi();
						}
						?>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php
					$filter->show_filter();
					get_template_part( 'template-parts/company', 'vacancies' );
					?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
