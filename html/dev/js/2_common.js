jQuery(document).ready(function($){
    if ($('.video').length) {
        $("video").click(function () {
            if (this.paused) {
                $("video").trigger("pause");
                $("video")
                    .parent()
                    .addClass("stop");
                $(this)
                    .parent()
                    .removeClass("stop");
                this.play();
            } else {
                $(this)
                    .parent()
                    .addClass("stop");
                this.pause();
            }
        });
    }
    $('p').filter(function(){
        return this.innerHTML == '&nbsp;';
    }).remove();
    $('.blog-inner p img').each(function(){
        $(this).unwrap();
    });
    $('header .logo').after('<div class="burger-menu"><span></span><span></span><span></span></div>')
    if($(window).width() < 768){
        $('header .menu').append('<li class="form"></li>')
        $('.form-search').appendTo('.form');
    }
    
    $(window).on('resize', function () {
        if ($(window).width() < 768) {
            if($('header .menu .form').length === 0){
                $('header .menu').append('<li class="form"></li>')
                $('.form-search').appendTo('.form');
            }
        } else {
            if($('.dfr > .form-search').length === 0){
                $('.menu .form-search').appendTo('header .dfr'); 
                $('header .menu .form').remove();
            }
        }
    });
    $('.burger-menu').click(function(){
        $('header .menu').toggleClass('open');
        $('body').toggleClass('modal-open');
        $(this).toggleClass('close');
    });
    if($('.tabs').length){
        $('.tabs li').each(function(){
            if($(this).hasClass('active')){
                var activeEl = $(this).children().attr('href');
                $(activeEl).addClass('active');
            }
        });
        $('.tabs a').click(function(e){
            e.preventDefault();
            var activeEl = $(this).attr('href');
            $('.row.dfr [class*="col-"]').removeClass('active');
            $(activeEl).addClass('active');
            if($(this).parent().hasClass('active')){
                $(this).parent().removeClass('active');
            }else{
                $('.tabs li').removeClass('active');
                $(this).parent().addClass('active');
            }
        });
    }
    $('.form-search .icon-search').click(function(e){
        if($(this).parents('.form-search').hasClass('open')){
            $(this).parents('.form-search').removeClass('open')
        }else{
            $(this).parents('.form-search').addClass('open')
            e.preventDefault();
        }
    });
    $(document).mouseup(function (e)  {
        var folder = $(".form-search");
        if (!folder.is(e.target) && folder.has(e.target).length === 0) {
            folder.removeClass('open');
        }
    });
})