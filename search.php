<?php
/**
 * Search Template.
 *
 * @package PR
 */

get_header();
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					if ( ! empty( $ads_top ) ) :
						get_template_part(
							'template-parts/ads',
							'header',
							[
								'ads_top' => $ads_top,
							]
						);
					endif;
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="title"><?php esc_html_e( 'Найдено результатов', 'pr' ); ?></h1>
				</div>
			</div>
			<div class="row dfr">
				<?php if ( have_posts() ) : ?>
					<?php
					while ( have_posts() ) :
						the_post();
						$category_post_id = get_the_ID();
						?>
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
							<div class="category-item">
								<?php if ( has_post_thumbnail( $category_post_id ) ) : ?>
									<?php the_post_thumbnail( 'pr_small_thumb' ); ?>
								<?php else : ?>
									<img
											width="180"
											height="120"
											src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
											alt="No Image">
								<?php endif; ?>
								<div class="description">
									<h2 class="title"><?php the_title(); ?></h2>
									<p class="date"><?php do_action( 'pr_the_time', $category_post_id ); ?></p>
								</div>
								<a class="link" href="<?php the_permalink(); ?>"></a>
							</div>
						</div>
					<?php endwhile; ?>
				<?php else : ?>
					<h2><?php esc_html_e( 'Нечего не найдено!', 'pr' ); ?></h2>
				<?php endif; ?>
			</div>
			<div class="page_navigation_wrapper">
				<?php
				if ( function_exists( 'wp_pagenavi' ) ) {
					wp_pagenavi();
				}
				?>
			</div>
		</div>
	</section>
<?php
get_footer();
