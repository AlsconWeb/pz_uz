<?php
/**
 * Functions file from theme PR.
 *
 * @package PR
 */

use PR\ThemeInit;

require_once __DIR__ . '/vendor/autoload.php';

new ThemeInit();
