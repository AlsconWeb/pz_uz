<?php
/**
 * Header jobs.
 *
 * @package alexlavigin/pr-theme
 */

$button_text = carbon_get_theme_option( 'pr_button_text' );
$button_link = carbon_get_theme_option( 'pr_button_link' );

$body_class = '';
if ( is_front_page() ) {
	$body_class = 'home';
}

if ( is_category() || is_home() ) {
	$body_class = 'category-page';
}

if ( is_tax( 'company' ) ) {
	$body_class = 'company';
}

if ( is_post_type_archive( 'jobs' ) || is_page( [ 3555, 5745 ] ) ) {
	$body_class = 'jobs-page';
}

if ( is_singular( 'jobs' ) ) {
	$body_class = 'jobs-inner';
}

?>

<!DOCTYPE html>
<html
	<?php language_attributes(); ?>
		style="margin-top: 0 !important;"
	<?php echo ! is_front_page() ? 'class="line"' : ''; ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
	<?php echo carbon_get_theme_option( 'pr_header_script' ); ?>
	<!--Реклама-->
	<script>window.yaContextCb = window.yaContextCb || []</script>
	<script src="https://yandex.ru/ads/system/context.js" async></script>
	<!--Реклама-->
</head>
<body <?php body_class( $body_class ); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HSGZ6Q"
			height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="dfr">
					<a class="logo" href="<?php echo esc_url( get_post_type_archive_link( 'jobs' ) ); ?>">
						<img
								src="/wp-content/uploads/2022/11/jobs_logo.svg"
								alt="Logo"
								width="244px">
					</a>
					<?php if ( has_nav_menu( 'header_menu_jobs' ) ) : ?>
						<?php
						wp_nav_menu(
							[
								'theme_location' => 'header_menu_jobs',
								'menu_class'     => 'menu',
								'container'      => '',
								'echo'           => true,
								'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							]
						);
						?>
					<?php endif; ?>
					<a class="button icon-telegram" href="<?php echo esc_url( $button_link ); ?>">
						<?php echo esc_html( $button_text ); ?>
					</a>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
</header>