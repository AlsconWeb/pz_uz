<?php
/**
 * 404 Error Template.
 *
 * @package PR
 */

get_header( '404' );
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul>
						<li>
							<?php echo wp_kses_post( carbon_get_theme_option( 'pr_404_block_first' ) ); ?>
						</li>
						<li>
							<?php echo wp_kses_post( carbon_get_theme_option( 'pr_404_block_second' ) ); ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( '404' );
