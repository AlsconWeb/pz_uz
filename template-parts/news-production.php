<?php
/**
 * Main page news production block template part.
 *
 * @package alexlavigin/pr-theme
 */

$term_id              = get_term_by( 'slug', 'production', 'post_tag' )->term_id;
$production_term_link = get_term_link( $term_id, 'post_tag' );

$arg = [
	'post_type'           => 'post',
	'posts_per_page'      => 3,
	'ignore_sticky_posts' => true,
	'post_status'         => 'publish',
	'tax_query'           => [
		[
			'taxonomy' => 'post_tag',
			'field'    => 'id',
			'terms'    => $term_id,
		],
	],
];

$production_query = new WP_Query( $arg );
?>
<div class="dfr">
	<h2><?php esc_html_e( 'Продакшен', 'pr' ); ?></h2>
	<a class="all-post" href="<?php echo esc_url( $production_term_link ); ?>">
		<?php esc_html_e( 'Больше продакшена', 'pr' ); ?>
	</a>
</div>
<div class="post-items full">
	<?php
	if ( $production_query->have_posts() ) {
		while ( $production_query->have_posts() ) {
			$production_query->the_post();
			$production_id = get_the_ID();
			$news_term     = wp_get_post_terms( $production_id, 'category' )[0];
			?>
			<div class="post-item">
				<a href="<?php the_permalink(); ?>">
					<?php
					if ( has_post_thumbnail( $production_id ) ) {
						the_post_thumbnail( 'pr_prod_thumb' );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
								alt="<?php esc_html( get_the_title( $production_id ) ); ?>">
					<?php } ?>
				</a>
				<div class="desc">
					<a class="link" href="<?php the_permalink(); ?>"> </a>
					<h3><?php the_title(); ?></h3>
					<p><?php the_excerpt(); ?></p>
					<p class="meta">
						<?php do_action( 'pr_the_time', $production_id ); ?>
						<a
								class="tag-link"
								data-cat="<?php echo esc_html( $news_term->name ); ?>"
								href="<?php echo esc_url( get_term_link( $news_term->term_id, 'category' ) ); ?>"></a>
					</p>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	}
	?>
</div>
