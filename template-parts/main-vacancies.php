<?php
/**
 * Main page vacancies block template part.
 *
 * @package alexlavigin/pr-theme
 */

use PR\ThemeInit;

$arg = [
	'post_type'      => 'jobs',
	'post_status'    => 'publish',
	'orderby'        => 'meta_value',
	'posts_per_page' => 5,
	'meta_key'       => '_pr_premium',
	'meta_query'     => [
		'relation' => 'OR',
		[
			'key'   => '_pr_premium',
			'value' => 'yes',
		],
		[
			'key'   => '_pr_premium',
			'value' => '',
		],
	],
];

$jobs = new WP_Query( $arg );
?>
<div class="vacancies-block">
	<h2><?php esc_html_e( 'Вакансии', 'pr' ); ?></h2>
	<?php
	if ( $jobs->have_posts() ) {
		?>
		<div class="vacancies">
			<?php
			while ( $jobs->have_posts() ) {
				$jobs->the_post();
				$job_id        = get_the_ID();
				$company_tem   = wp_get_post_terms( $job_id, 'company', [ 'fields' => 'all' ] )[0];
				$company_logo  = carbon_get_term_meta( $company_tem->term_id, 'pr_company_logo' );
				$city          = wp_get_post_terms( $job_id, 'location', [ 'fields' => 'all' ] )[0];
				$work_day      = carbon_get_post_meta( $job_id, 'pr_working' );
				$job_condition = ThemeInit::get_condition( $job_id );
				$job_salary    = ThemeInit::get_salary( $job_id );
				$premium       = carbon_get_post_meta( $job_id, 'pr_premium' );
				?>
				<div class="vacancy <?php echo $premium ? 'premium' : ''; ?>">
					<h5 class="title" <?php echo $premium ? 'data-title="Premium"' : ''; ?>>
						<?php echo esc_html( $company_tem->name ?? '' ); ?>
					</h5>
					<?php if ( ! empty( $company_logo ) ) { ?>
						<img
								width="100px"
								src="<?php echo esc_url( $company_logo ); ?>"
								alt="<?php echo esc_html( 'logo ' . $company_tem->name ); ?>">
					<?php } else { ?>
						<img src="https://via.placeholder.com/70x50" alt="No company logo">
					<?php } ?>
					<h3 class="title"><?php the_title(); ?></h3>
					<p class="city">
						<?php echo esc_html( $city->name ?? '' ); ?>
						• <?php echo esc_html( ThemeInit::get_work_day( $work_day ) ); ?>
					</p>
					<p class="price"><?php echo esc_html( $job_salary ); ?></p>
					<a class="link" href="<?php the_permalink(); ?>"></a>
				</div>
			<?php } ?>
			<a class="button" href="<?php echo esc_url( get_post_type_archive_link( 'jobs' ) ); ?>">
				<?php esc_html_e( 'Больше вакансий →', 'pr' ); ?>
			</a>
		</div>
		<?php
	}
	?>
</div>
