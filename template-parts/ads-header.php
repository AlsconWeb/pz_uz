<?php
/**
 * Ads Header template.
 *
 * @package PR
 */

?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="advertising">
			<?php echo $args['ads_top']; ?>
		</div>
	</div>
</div>
