<?php
/**
 * Template part ads fox.
 *
 * @package alexlavigin/pr-theme
 */
?>

<div class="advertising <?php echo esc_attr( $args['class_name'] ); ?> <?php echo esc_attr( $args['position'] ); ?>">
	<!--AdFox START-->
	<!--WebSail-Advertisement-->
	<!--Площадка: PR.UZ / * / *-->
	<!--Тип баннера: 1170x250-->
	<!--Расположение: верх страницы-->
	<div id="<?php echo esc_attr( $args['block_id'] ); ?>"></div>
	<script>
		window.yaContextCb.push( () => {
			Ya.adfoxCode.create( {
				ownerId: 277709,
				containerId: '<?php echo esc_attr( $args['block_id'] ); ?>',
				params: {
					pp: 'g',
					ps: 'fyae',
					p2: 'hpfv',
					puid1: ''
				}
			} )
		} )
	</script>
</div>
