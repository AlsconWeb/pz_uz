<?php
/**
 * Company vacancies template part.
 *
 * @package alexlavigin/pr-theme
 */

$company_terms = get_terms(
	[
		'taxonomy'   => 'company',
		'hide_empty' => false,
		'number'     => 5,
		'orderby'    => 'count',
		'order'      => 'DESC',
	]
);

?>
<div class="company-vacancies">
	<h3 class="title"><?php esc_html_e( 'Вакансии компаний', 'pr' ); ?></h3>
	<?php
	if ( ! empty( $company_terms ) ) {
		foreach ( $company_terms as $company ) {
			$company_logo = carbon_get_term_meta( $company->term_id, 'pr_company_logo' );
			?>
			<div class="vacancy">
				<?php if ( ! empty( $company_logo ) ) { ?>
					<img src="<?php echo esc_url( $company_logo ); ?>" alt="<?php echo esc_html( $company->name ); ?>">
					<?php
				} else {
					?>
					<img src="https://via.placeholder.com/70x50" alt="No company logo">
				<?php } ?>
				<div class="desc">
					<h3 class="title"><?php echo esc_html( $company->name ); ?></h3>
					<?php
					echo sprintf(
						'<p>%s: %s</p>',
						esc_html( __( 'Вакансий', 'pr' ) ),
						esc_attr( $company->count )
					);
					?>
				</div>
				<a class="link" href="<?php echo esc_url( get_term_link( $company->term_id ) ); ?>"></a>
			</div>
			<?php
		}
		?>
		<a
				class="button"
				href="<?php echo esc_url( get_the_permalink( 5745 ) ); ?>">
			<?php esc_html_e( 'Все компании', 'pr' ); ?>
		</a>
		<?php
	} else {
		?>
		<h5><?php esc_attr_e( 'Создайте компанию', 'pr' ); ?></h5>
	<?php } ?>
</div>
