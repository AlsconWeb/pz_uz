<?php
/**
 * Main page news tenders block template part.
 *
 * @package alexlavigin/pr-theme
 */

$term_id           = get_term_by( 'slug', 'tenders', 'category' )->term_id;
$tenders_term_link = get_term_link( $term_id, 'category' );

$arg = [
	'post_type'           => 'post',
	'posts_per_page'      => 4,
	'ignore_sticky_posts' => true,
	'post_status'         => 'publish',
	'tax_query'           => [
		[
			'taxonomy' => 'category',
			'field'    => 'id',
			'terms'    => $term_id,
		],
	],
];

$tenders_query = new WP_Query( $arg );
?>
<div class="tenders-block">
	<div class="dfr">
		<h2><?php esc_html_e( 'ТЕНДЕРЫ', 'pr' ); ?></h2>
		<a class="all-post" href="<?php echo esc_url( $tenders_term_link ); ?>">
			<?php esc_html_e( 'Все тендеры', 'pr' ); ?>
		</a>
	</div>
	<div class="post-items tenders">
		<?php
		if ( $tenders_query->have_posts() ) {
			while ( $tenders_query->have_posts() ) {
				$tenders_query->the_post();
				$tender_id = get_the_ID();
				?>
				<div class="post-item">
					<a href="<?php the_permalink(); ?>">
						<?php
						if ( has_post_thumbnail( $tender_id ) ) {
							the_post_thumbnail( 'pr_tenders_thumb' );
						} else {
							?>
							<img
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
									alt="<?php esc_html( get_the_title( $tender_id ) ); ?>">
						<?php } ?>
					</a>
					<div class="desc">
						<h3><?php the_title(); ?></h3>
						<p><?php do_action( 'pr_the_time', $tender_id ); ?></p>
						<a class="link" href="<?php the_permalink(); ?>"></a>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>
