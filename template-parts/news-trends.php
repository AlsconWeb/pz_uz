<?php
/**
 * Main page news trends block template part.
 *
 * @package alexlavigin/pr-theme
 */

$sticky = get_option( 'sticky_posts' );
rsort( $sticky );
$sticky = array_slice( $sticky, 0, 10 );
$arg    = [
	'post__in'            => $sticky,
	'posts_per_page'      => 9,
	'orderby'             => 'post__in',
	'post_status'         => 'publish',
	'ignore_sticky_posts' => true,
];

$news_trends_query = new WP_Query( $arg );

$i = 0;
?>

<div class="dfr">
	<h2><?php esc_html_e( 'В тренде', 'pr' ); ?></h2>
	<a class="all-post" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>">
		<?php esc_html_e( 'Все новости', 'pr' ); ?>
	</a>
</div>
<div class="post-items">
	<?php
	if ( $news_trends_query->have_posts() ) {
		while ( $news_trends_query->have_posts() ) {
			$i ++;
			$news_trends_query->the_post();
			$news_trends_id = get_the_ID();
			$news_term      = wp_get_post_terms( $news_trends_id, 'category' )[0];
			$image_size     = 3 >= $i ? 'pr_thumb' : 'pr_small_thumb';
			?>
			<div class="post-item">
				<a href="<?php the_permalink(); ?>">
					<?php
					if ( has_post_thumbnail( $news_trends_id ) ) {
						the_post_thumbnail( $image_size );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
								alt="<?php esc_html( get_the_title( $news_trends_id ) ); ?>">
					<?php } ?>
				</a>
				<div class="desc">
					<h3>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h3>
					<p data-cat="<?php echo esc_html( $news_term->name ); ?>">
						<?php do_action( 'pr_the_time', $news_trends_id ); ?>
						<a
								class="tag-link"
								data-cat="<?php echo esc_html( $news_term->name ); ?>"
								href="<?php echo esc_url( get_term_link( $news_term->term_id, 'category' ) ); ?>"></a>
					</p>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	}
	?>
</div>
