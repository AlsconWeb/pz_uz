<?php
/**
 * Template Part: Popular Posts.
 *
 * @package PR
 */

?>
<h3 class="title"><?php esc_html_e( 'Популярные новости', 'pr' ); ?></h3>
<?php

$popular_arg = [
	'post_type'      	  => 'post',
	'post_status'    	  => 'publish',
	'post__not_in'   	  => $args['execute'],
	'posts_per_page' 	  => $args['posts_per_page'] ?? 9,
	'meta_key'			  => 'pr_views',
	'orderby' 			  => 'meta_value_num',
	'order'   			  => 'DESC',
	'ignore_sticky_posts' => 1,
	'meta_query' 		  => [
		[
			'key'  => 'pr_views',
			'type' => 'UNSIGNED',
		],
	],
	'tax_query'    		 => [
		'taxonomy' => 'post_format',
		'field'    => 'slug',
		'terms'    => [ 'post-format-video', 'post-format-audion' ],
		'operator' => 'NOT IN',
	],
];

$popular_query = new WP_Query( $popular_arg );
?>
<div class="blog-items" data-title="">
	<?php
	if ( $popular_query->have_posts() && 9 === $popular_query->post_count ) {
		while ( $popular_query->have_posts() ) {
			$popular_query->the_post();
			$popular_pots_id = get_the_ID();
			?>
			<div class="blog-item">
				<div class="description">
					<h2 class="title"><?php the_title(); ?></h2>
				</div>
				<?php if ( has_post_thumbnail( $popular_pots_id ) ) : ?>
					<?php the_post_thumbnail( 'pr_small_thumb' ); ?>
				<?php else : ?>
					<img
							src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
							alt="No Image">
				<?php endif; ?>
				<a class="link" href="<?php the_permalink(); ?>"></a>
			</div>
			<?php
		}
		wp_reset_postdata();
	} else {
		$popular_arg = [
			'post_type'      	  => 'post',
			'post_status'    	  => 'publish',
			'post__not_in'   	  => $args['execute'],
			'posts_per_page' 	  => $args['posts_per_page'] ?? 9,
			'tax_query'    		  => [
				'taxonomy' => 'post_format',
				'field'    => 'slug',
				'terms'    => [ 'post-format-video', 'post-format-audion' ],
				'operator' => 'NOT IN',
			],
			'order'		   		=> 'DESC',
			'orderby'      		=> 'date',
		];

		$last = new WP_Query( $popular_arg );
		if ( $last->have_posts() ) {
			while ( $last->have_posts() ) {
				$last->the_post();
				$last_pots_id = get_the_ID();
				?>
				<div class="blog-item">
					<div class="description">
						<h2 class="title"><?php the_title(); ?></h2>
					</div>
					<?php if ( has_post_thumbnail( $last_pots_id ) ) : ?>
						<?php the_post_thumbnail( 'pr_small_thumb' ); ?>
					<?php else : ?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
								alt="No Image">
					<?php endif; ?>
					<a class="link" href="<?php the_permalink(); ?>"></a>
				</div>
				<?php
			}
			wp_reset_postdata();
		}
	}

	if ( $args['read_more'] ) {

		?>
		<a class="readmore" href="<?php echo esc_url( get_post_type_archive_link( 'post' ) . '?popular=true' ); ?>">
			<?php esc_html_e( 'Больше новостей', 'pr' ); ?>
		</a>
		<?php
	}
