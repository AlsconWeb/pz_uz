<?php
/**
 * Main page news slider block template part.
 *
 * @package alexlavigin/pr-theme
 */

$term_id = get_term_by( 'slug', 'advertising', 'category' )->term_id;

$arg = [
	'post_type'           => 'post',
	'posts_per_page'      => 12,
	'ignore_sticky_posts' => true,
	'tax_query'           => [
		[
			'taxonomy' => 'category',
			'field'    => 'id',
			'terms'    => $term_id,
		],
	],
];

$ads_query = new WP_Query( $arg );
?>
<div class="post-items slider">
	<?php
	if ( $ads_query->have_posts() ) {
		while ( $ads_query->have_posts() ) {
			$ads_query->the_post();
			$interview_id = get_the_ID();
			?>
			<div class="post-item">
				<a href="<?php the_permalink(); ?>">
					<?php
					if ( has_post_thumbnail( $interview_id ) ) {
						the_post_thumbnail( 'pr_asd_thumb' );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
								alt="<?php esc_html( get_the_title( $interview_id ) ); ?>">
					<?php } ?>
				</a>
				<div class="desc">
					<h3>
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h3>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	}
	?>
</div>
