<?php
/**
 * Main page news Interview block template part.
 *
 * @package alexlavigin/pr-theme
 */

$term_id             = get_term_by( 'slug', 'interview', 'post_tag' )->term_id;
$interview_term_link = get_term_link( $term_id, 'post_tag' );

$arg = [
	'post_type'           => 'post',
	'posts_per_page'      => 3,
	'ignore_sticky_posts' => true,
	'post_status'         => 'publish',
	'tax_query'           => [
		[
			'taxonomy' => 'post_tag',
			'field'    => 'id',
			'terms'    => $term_id,
		],
	],
];

$interview_query = new WP_Query( $arg );

?>
<div class="dfr">
	<h2><?php esc_html_e( 'Интервью', 'pr' ); ?></h2>
	<a class="all-post" href="<?php echo esc_url( $interview_term_link ); ?>">
		<?php esc_html_e( 'Все интервью', 'pr' ); ?>
	</a>
</div>
<div class="post-items">
	<?php
	if ( $interview_query->have_posts() ) {
		while ( $interview_query->have_posts() ) {
			$interview_query->the_post();

			$interview_id = get_the_ID();
			$news_term    = wp_get_post_terms( $interview_id, 'category' )[0];
			?>
			<div class="post-item">
				<a href="#">
					<?php
					if ( has_post_thumbnail( $interview_id ) ) {
						the_post_thumbnail( 'pr_thumb' );
					} else {
						?>
						<img
								src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
								alt="<?php esc_html( get_the_title( $interview_id ) ); ?>">
					<?php } ?>
				</a>
				<div class="desc">
					<h3>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h3>
					<p>
						<?php do_action( 'pr_the_time', $interview_id ); ?>
						<a
								class="tag-link"
								data-cat="<?php echo esc_html( $news_term->name ); ?>"
								href="<?php echo esc_url( get_term_link( $news_term->term_id, 'category' ) ); ?>"></a>
					</p>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	}
	?>
</div>
