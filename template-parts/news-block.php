<?php
/**
 * Main page news block template part.
 *
 * @package alexlavigin/pr-theme
 */

$args = [
	'post_type'           => 'post',
	'ignore_sticky_posts' => true,
	'order'               => 'DESC',
	'orderby'             => 'date',
	'posts_per_page'      => 14,
];

$news_query = new WP_Query( $args );
$i          = 0;
?>
<div class="post-items">
	<?php
	if ( $news_query->have_posts() ) {
		while ( $news_query->have_posts() ) {
			$news_query->the_post();
			$news_id = get_the_ID();
			?>
			<div class="post-item">
				<?php
				$i ++;
				if ( $i % 3 === 0 ) {
					?>
					<a href="<?php the_permalink(); ?>">
						<?php
						if ( has_post_thumbnail( $news_id ) ) {
							the_post_thumbnail( 'pr_news_small_thumb' );
						} else {
							?>
							<img
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/no_image.png' ); ?>"
									alt="<?php esc_html( get_the_title( $news_id ) ); ?>">
							<?php
						}
						?>
					</a>
				<?php } ?>
				<div class="desc">
					<h3>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h3>
					<p><?php do_action( 'pr_the_time', $news_id ); ?></p>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	} else {
		?>
		<h3><?php esc_html_e( 'Статей не найдено', 'pr' ); ?></h3>
	<?php } ?>
</div>
<a class="button" href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' ) ) ); ?>">
	<?php esc_html_e( 'Все новости', 'pr' ); ?>
</a>
