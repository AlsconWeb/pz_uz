<?php
/**
 * Search form template.
 *
 * @package PR
 */

?>
<form class="form-search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input
			class="search" type="text" name="s" value="<?php echo get_search_query(); ?>"
			placeholder="<?php esc_html_e( 'Поиск', 'pr' ); ?>">
	<button class="icon-search"></button>
</form>
