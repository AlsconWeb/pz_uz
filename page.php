<?php
/**
 * Page Template.
 *
 * @package PR
 */

get_header();

?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<?php if ( have_posts() ) : ?>
				<div class="row">
					<?php
					while ( have_posts() ) :
						the_post();
						?>
						<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
							<?php the_content(); ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php
get_footer();
